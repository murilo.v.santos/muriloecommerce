using System.Linq;

public class ClienteMemoryRepository : IClienteRepository
{
    List<Cliente> clientes = new List<Cliente>();

    public void Create(Cliente cliente)
    {
        clientes.Add(cliente);
    }

    public void Delete(int id)
    {
       var cliente = (from c in clientes
                        where c.ClienteId == id
                        select c)
                        .FirstOrDefault();
        clientes.Remove(cliente);
    }

    public List<Cliente> Read()
    {
        return clientes;
    }

    public Cliente Read(int id)
    {
        return clientes.FirstOrDefault(c => c.ClienteId == id);
    }

    public List<Cliente> Search(string pesquisa)
    {
        List<Cliente> listaFiltrada = clientes.Where(c => c.Name.Contains(pesquisa)).ToList();
        return listaFiltrada;
    }

    public void Update(Cliente cliente)
    {
        var c = Read(cliente.ClienteId);

        c.Name = cliente.Name;
        c.Email = cliente.Email;
        c.Password = cliente.Password;
    }
}